using UnityEngine;

namespace EasyScelet.Extension {
    public static class VectorExtension {
        public static Vector3 GetSignVector (this Vector3 vector) {
            return new Vector3 (Mathf.Sign (vector.x), Mathf.Sign (vector.y), Mathf.Sign (vector.z));
        }
    }
}