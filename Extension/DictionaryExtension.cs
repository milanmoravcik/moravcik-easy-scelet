using System.Collections.Generic;

namespace EasyScelet.Extension {
    public static class DictionaryExtension {

        public static VALUE GetOrNull<KEY, VALUE> (this Dictionary<KEY, VALUE> dictionary, KEY key) {
            return dictionary.ContainsKey (key) ? dictionary[key] : default (VALUE);
        }
    }
}
