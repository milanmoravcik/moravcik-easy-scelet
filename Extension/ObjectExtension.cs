using System;

namespace EasyScelet.Extension {
    public static class ObjectExtension {

        public static TYPE Cast<TYPE> (this object obj) {
            return (TYPE) obj;
        }

        public static string ToStringSafe (this object obj) {
            return null == obj ? "null" : obj.ToString ();
        }
    }
}
