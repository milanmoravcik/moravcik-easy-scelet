using System;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

namespace EasyScelet.Extension {
    public static class IDictionaryExtension {

        public static void ThrowIfDoesNotExist (this IDictionary dictionary, string key) {
            if (!dictionary.Contains (key)) {
                throw new Exception (string.Format ("value under key ({0}) doesn't exist in dictionary ({1})", key, Json.Serialize (dictionary)));
            }
        }

        public static TYPE GetOrDefault<TYPE> (this IDictionary dictionary, string key, TYPE defaultValue) {
            if (dictionary.Contains (key)) {
                return GetOrNull<TYPE> (dictionary, key);
            }
            return defaultValue;
        }

        public static TYPE GetOrNull<TYPE> (this IDictionary dictionary, string key) {
            if (typeof (TYPE) == typeof (System.Single)) {
                return System.Convert.ToSingle (dictionary[key]).Cast<TYPE> ();
            } else if (typeof (TYPE) == typeof (float)) {
                return ((float)(double)dictionary[key]).Cast<TYPE> ();
            } else if (typeof (TYPE) == typeof (float?)) {
                object obj = dictionary[key];
                return null == obj ? obj.Cast<TYPE> () : ((float?)(float)((double?)obj).Value).Cast<TYPE> ();
            } else if (typeof (TYPE) == typeof (int)) {
                return ((int)dictionary[key]).Cast<TYPE> ();
            }
            return (TYPE)dictionary[key];
        }

        public static TYPE GetOrThrow<TYPE> (this IDictionary dictionary, string key) {
            ThrowIfDoesNotExist (dictionary, key);
            return GetOrNull<TYPE> (dictionary, key);
        }

        public static IDictionary Clone<KEY, VALUE> (this IDictionary dictionary) {
            return (IDictionary)new Dictionary<KEY, VALUE> ((IDictionary<KEY, VALUE>)dictionary);
        }
    }
}
