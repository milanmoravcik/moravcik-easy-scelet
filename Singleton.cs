using UnityEngine;

namespace EasyScelet {
    public class Singleton<CLASS> where CLASS : new() {
        private static CLASS _instance;
        private static object _syncRoot = new object();

        protected Singleton () {
        }

        public static CLASS INSTANCE {
            get {
                if (null == _instance) {
                    lock (_syncRoot) {
                        if (null == _instance) {
                            _instance = new CLASS();
                        }
                    }
                }
                return _instance;
            }
        }
    }

    public class SingletonMonoBehaviourDestroy<CLASS> : MonoBehaviour where CLASS : Component {
        private static CLASS instance;
        public static CLASS INSTANCE {
            get {
                if (instance == null) {
                    instance = FindObjectOfType<CLASS>();
                    if (instance == null) {
                        GameObject obj = new GameObject();
                        obj.name = typeof(CLASS).Name;
                        instance = obj.AddComponent<CLASS>();
                    }
                }
                return instance;
            }
        }

        protected virtual void Awake () {
            if (instance == null) {
                instance = this as CLASS;
            } else {
                Destroy(gameObject);
            }
        }
    }

    public class SingletonMonoBehaviour<CLASS> : MonoBehaviour where CLASS : Component {
        private static CLASS instance;
        public static CLASS INSTANCE {
            get {
                if (instance == null) {
                    instance = FindObjectOfType<CLASS>();
                    if (instance == null) {
                        GameObject obj = new GameObject();
                        obj.name = typeof(CLASS).Name;
                        instance = obj.AddComponent<CLASS>();
                    }
                }
                return instance;
            }
        }

        protected virtual void Awake () {
            if (instance == null) {
                instance = this as CLASS;
                DontDestroyOnLoad(this.gameObject);
            } else {
                Destroy(gameObject);
            }
        }
    }
}
