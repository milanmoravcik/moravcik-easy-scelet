using UnityEngine;
using System.Collections;
using EasyScelet.Extension;

namespace EasyScelet.Common {
	public class MonoHandler: MonoBehaviour {

		protected bool _horizontalFlip = false;

        public COMPONENT CheckAndAddComponent<COMPONENT>() where COMPONENT : Component {
            COMPONENT component = GetComponent<COMPONENT>();
            if (null == component) {
                component = gameObject.AddComponent<COMPONENT>();
            }
            return component;
        }

        public virtual bool HorizontalFlip {
			get {
				return _horizontalFlip;
			}
		
			set { 
				if (value != _horizontalFlip) {
					_horizontalFlip = value;
					FlipHorizontaly();
				}
			}
		}

		public void SetLocalPosition(Vector3 localPosition) {
			transform.localPosition = localPosition;
		}

        public void SetName(string name) {
            transform.name = name;
        }

        public virtual void Initialize(IDictionary configurationDict) {
            IDictionary localPositionDict = configurationDict.GetOrNull<IDictionary>("local-position");
            if (null != localPositionDict) {
                float x = localPositionDict.GetOrThrow<float>("x");
                float y = localPositionDict.GetOrThrow<float>("y");
                transform.localPosition = new Vector3(x, y);
            }

            bool? horizontalFlip = configurationDict.GetOrNull<bool?>("flip-horizontal");
            if (null != horizontalFlip) {
                HorizontalFlip = horizontalFlip.Value;
            }

            float? scale = configurationDict.GetOrNull<float?>("scale");
            if (null != scale) {
                SetScale(scale.Value);
            }
        }

        public void FlipHorizontaly() {
			Vector3 scale = transform.localScale;
			scale.x *= -1;
			transform.localScale = scale;
		}
	
	
		public void SetScale(float scale) {
			transform.localScale = transform.localScale.GetSignVector()*scale;
		}
	
		public void SetParent(Transform inputTransform) {
            bool areEquals = inputTransform == null ? this.transform.parent == null : inputTransform.Equals(this.transform.parent);
            if (!areEquals) {
				this.transform.parent = inputTransform;
			}
		}
	}
}
