﻿using System;
using UnityEngine;

namespace EasyScelet.Common {
    public class PlayerPrefsManager : SingletonMonoBehaviour<PlayerPrefsManager> {

        public bool HasKey (string key) {
            return PlayerPrefs.HasKey (key);
        }

        public void SetString (string key, string value) {
            PlayerPrefs.SetString (key, value);
        }

        public void SetInt (string key, int value) {
            PlayerPrefs.SetInt (key, value);
        }

        public void SetFloat (string key, float value) {
            PlayerPrefs.SetFloat (key, value);
        }

        public void SetBool (string key, bool value) {
            PlayerPrefs.SetInt (key, value ? 1 : 0);
        }

        public string GetString (string key) {
            return HasKey (key) ? PlayerPrefs.GetString (key) : throw new Exception (string.Format ("Value under key ({0}) doesn't exist.", key));
        }

        public string GetStringOrDefault (string key, string defaultValue) {
            return HasKey (key) ? PlayerPrefs.GetString (key) : defaultValue;
        }

        public int GetInt (string key) {
            return HasKey (key) ? PlayerPrefs.GetInt (key) : throw new Exception (string.Format ("Value under key ({0}) doesn't exist.", key));
        }
        public int GetIntOrDefault (string key, int defaultValue) {
            return HasKey (key) ? PlayerPrefs.GetInt (key) : defaultValue;
        }

        public float GetFloat (string key) {
            return HasKey (key) ? PlayerPrefs.GetFloat (key) : throw new Exception (string.Format ("Value under key ({0}) doesn't exist.", key));
        }
        public float GetFloatOrDefault (string key, float defaultValue) {
            return HasKey (key) ? PlayerPrefs.GetFloat (key) : defaultValue;
        }

        public bool GetBool (string key) {
            return HasKey (key) ? PlayerPrefs.GetInt (key, 1) != 0 : throw new Exception (string.Format ("Value under key ({0}) doesn't exist.", key));
        }

        public bool GetBoolOrDefault (string key, bool defaultValue) {
            return HasKey (key) ? PlayerPrefs.GetInt (key, 1) != 0 : defaultValue;
        }

        public void DeleteAll () {
            PlayerPrefs.DeleteAll ();
        }

        public void DeleteKey (string key) {
            PlayerPrefs.DeleteKey (key);
        }

        private void OnApplicationQuit () {
            PlayerPrefs.Save ();
        }
    }
}
