using System;
using System.Collections.Generic;
using EasyScelet.Extension;

namespace EasyScelet.Common.Messages {
    public class MessagesManager : SingletonMonoBehaviourDestroy<MessagesManager> {

        private Dictionary<string, List<IMessageReciever>> _messageRecieverListDict;

        protected override void Awake () {
            base.Awake ();
            _messageRecieverListDict = new Dictionary<string, List<IMessageReciever>> ();
        }

        public void SendMessageCommand (string messageName, string messageCommand, MonoHandler monoHandler = null) {
            List<IMessageReciever> recieverList = _messageRecieverListDict.GetOrNull (messageName);
            if (recieverList != null) {
                foreach (IMessageReciever eventCommandListener in recieverList.ToArray ()) {
                    // remove null reference and skip to next
                    if ((UnityEngine.Object)eventCommandListener == null) {
                        recieverList.Remove (eventCommandListener);
                        continue;
                    }

                    eventCommandListener.OnMessage (messageName, messageCommand, monoHandler);
                }
            }
        }

        public void RegisterMessageReciever (String messageName, IMessageReciever IMessageReciever) {
            List<IMessageReciever> recieverList = _messageRecieverListDict.GetOrNull (messageName);
            if (null == recieverList) {
                recieverList = new List<IMessageReciever> ();
                _messageRecieverListDict[messageName] = recieverList;
            }
            recieverList.Add (IMessageReciever);
        }
    }
}
