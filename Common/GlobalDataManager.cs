﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyScelet.Common {
    public class GlobalDataManager : SingletonMonoBehaviour<GlobalDataManager> {

        public IDictionary dataDict;

        protected override void Awake () {
            base.Awake ();
            dataDict = new Dictionary<string, object> ();
        }

        public VALUE GetItemOrDefault<VALUE> (string item, VALUE defaultValue = default (VALUE)) {
            return (dataDict.Contains (item)) ? (VALUE)dataDict[item] : defaultValue;
        }

        public VALUE GetItemOrThrow<VALUE> (string item) {
            if (!dataDict.Contains (item)) {
                throw new Exception (string.Format ("Value under key ({0}) doesn't exist in GlobalData dictionary.", item));
            }
            return (VALUE)dataDict[item];
        }

        public void SetItem<VALUE> (string item, VALUE value) {
            dataDict[item] = value;
        }

        public void RemoveItem (string item) {
            if (dataDict.Contains (item)) {
                dataDict.Remove (item);
            }
        }
    }
}