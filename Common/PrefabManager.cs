﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace EasyScelet.Common {
    public class PrefabManager : SingletonMonoBehaviourDestroy<PrefabManager> {

        private Dictionary<string, GameObject> _prefabs;

        protected override void Awake () {
            base.Awake ();
            _prefabs = new Dictionary<string, GameObject> ();
        }
        
        public GameObject GetOrLoadPrefab (string prefabPath) {
            if (_prefabs.ContainsKey (prefabPath)) {
                return _prefabs[prefabPath];
            } else {
                GameObject prefab = Resources.Load<GameObject> (prefabPath);

                if (prefab) {
                    _prefabs.Add (prefabPath, prefab);
                    return prefab;
                } else {
                    throw new Exception (string.Format ("PrefabManager error - can't load prefab for path ({0})", prefabPath));
                }
            }
        }
    }
}