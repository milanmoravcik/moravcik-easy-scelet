using System;

namespace EasyScelet.Common.Messages {
    public interface IMessageReciever {
		void OnMessage(string messageName, string messageCommand, MonoHandler monoHandler);
	}
}

