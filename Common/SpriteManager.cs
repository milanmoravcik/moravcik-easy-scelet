﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using EasyScelet.Helper;
using System.Collections;

namespace EasyScelet.Common {
    public class SpriteManager : SingletonMonoBehaviourDestroy<SpriteManager> {
        private Dictionary<string, Sprite[]> _sprites;
        private Dictionary<string, Sprite> _spritesFromFiles;
        private Dictionary<string, Sprite> _spritesFromWebRequests;

        protected override void Awake () {
            base.Awake ();
            _sprites = new Dictionary<string, Sprite[]> ();
            _spritesFromFiles = new Dictionary<string, Sprite> ();
            _spritesFromWebRequests = new Dictionary<string, Sprite> ();
        }

        public Sprite[] GetOrLoadSprites (string spritesPath) {
            if (!_sprites.ContainsKey (spritesPath)) {
                Sprite[] sprites = Resources.LoadAll<Sprite> (spritesPath);
                if (sprites.Length < 1) {
                    throw new Exception (string.Format ("Can't load sprites for path ({0})", spritesPath));
                }
                _sprites.Add (spritesPath, sprites);
            }
            return _sprites[spritesPath];
        }

        private Sprite GetOrLoadSprite (string spriteName, string texturePath) {
            // check if sprite exists in directory
            if (_sprites.ContainsKey (texturePath)) {
                Sprite findedSprite = Array.Find<Sprite> (_sprites[texturePath], s => (s.name == spriteName));
                if (findedSprite == null) {
                    throw new Exception (string.Format ("Can't find sprite with name ({0}) in texture ({1})", spriteName, texturePath));
                }
                return findedSprite;
            }

            Sprite[] sprites = Resources.LoadAll<Sprite> (texturePath);
            sprites = Resources.LoadAll<Sprite> (texturePath);
            if (sprites.Length < 1) {
                throw new Exception (string.Format ("Can't find texture in path ({0})", texturePath));
            }
            _sprites.Add (texturePath, sprites);
            Sprite createdSprite = Array.Find<Sprite> (_sprites[texturePath], s => (s.name == spriteName));
            if (createdSprite == null) {
                throw new Exception (string.Format ("Can't find sprite with name ({0}) in texture ({1})", spriteName, texturePath));
            }

            return createdSprite;
        }

        public Sprite GetOrLoadSprite (string spriteReference) {
            // spriteReference == texturePath + spriteName
            int indexLastSep = spriteReference.LastIndexOf (PathHelper.DirSeparator);
            string texturePath = spriteReference.Substring (0, indexLastSep);
            string spriteName = spriteReference.Substring (indexLastSep + 1, spriteReference.Length - indexLastSep - 1);
            return GetOrLoadSprite (spriteName, texturePath);
        }

        public Sprite GetOrLoadSpriteFromFile (string filePath) {
            if (!_spritesFromFiles.ContainsKey (filePath)) {
                if (string.IsNullOrEmpty (filePath) || !System.IO.File.Exists (filePath)) {
                    throw new Exception (string.Format ("Can't not find file with path ({0})", filePath));
                }

                byte[] bytes = System.IO.File.ReadAllBytes (filePath);
                Texture2D texture = new Texture2D (1, 1);
                texture.LoadImage (bytes);
                Sprite sprite = Sprite.Create (texture, new Rect (0, 0, texture.width, texture.height), new Vector2 (0.5f, 0.5f));
                _spritesFromFiles.Add (filePath, sprite);

                return sprite;
            } else {
                return _spritesFromFiles[filePath];
            }
        }

        public IEnumerator GetOrLoadSpriteFromWebRequest (string filePath, Action<Sprite> spriteCallback) {
            if (!_spritesFromWebRequests.ContainsKey (filePath)) {
                using (UnityWebRequest webRequest = UnityWebRequest.Get (filePath)) {
                    yield return webRequest.SendWebRequest ();
                    if (!webRequest.isNetworkError) {

                        byte[] bytes = webRequest.downloadHandler.data;
                        Texture2D texture = new Texture2D (1, 1);
                        texture.LoadImage (bytes);
                        Sprite sprite = Sprite.Create (texture, new Rect (0, 0, texture.width, texture.height), new Vector2 (0.5f, 0.5f));
                        _spritesFromWebRequests.Add (filePath, sprite);
                        spriteCallback (sprite);
                    }
                }
            } else {
                spriteCallback (_spritesFromWebRequests[filePath]);
            }
        }
    }
}