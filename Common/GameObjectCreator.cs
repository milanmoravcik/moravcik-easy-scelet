﻿using System;
using UnityEngine;

namespace EasyScelet.Common {
    public class GameObjectCreator {


        public static MonoBehaviour CreateAndGetFromPrefab (string prefabRef, Transform parent = null) {
            GameObject source = PrefabManager.INSTANCE.GetOrLoadPrefab (prefabRef);

            if (source) {
                MonoBehaviour monoObject;
                if (parent != null) {
                    monoObject = GameObject.Instantiate (source, parent).GetComponent<MonoBehaviour> ();
                } else {
                    monoObject = GameObject.Instantiate (source).GetComponent<MonoBehaviour> ();
                }
                return monoObject;
            } else {
                throw new Exception (string.Format ("Source from reference ({0}) is null.", prefabRef));
            }
        }

        public static HANDLER CreateAndGetFromPrefab<HANDLER> (string prefabRef, Transform parent = null) where HANDLER : MonoBehaviour {
            GameObject source = PrefabManager.INSTANCE.GetOrLoadPrefab (prefabRef);

            if (source) {
                HANDLER handler;
                if (parent != null) {
                    handler = GameObject.Instantiate (source, parent).GetComponent<HANDLER> ();
                } else {
                    handler = GameObject.Instantiate (source).GetComponent<HANDLER> ();
                }
                return handler;
            } else {
                throw new Exception (string.Format ("Source from reference ({0}) is null.", prefabRef));
            }
        }
    }
}