﻿using UnityEngine;

namespace EasyScelet.Helper {
    public class PathHelper {

        private static string _dirSeparator = "/";

        public static string Join (string path1, string path2) {
            string[] arrayPaths = new string[] { path1, path2 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string Join (string path1, string path2, string path3) {
            string[] arrayPaths = new string[] { path1, path2, path3 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string Join (string path1, string path2, string path3, string path4) {
            string[] arrayPaths = new string[] { path1, path2, path3, path4 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string Join (string path1, string path2, string path3, string path4, string path5) {
            string[] arrayPaths = new string[] { path1, path2, path3, path4, path5 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string Join (string path1, string path2, string path3, string path4, string path5, string path6) {
            string[] arrayPaths = new string[] { path1, path2, path3, path4, path5, path6 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string Join (string path1, string path2, string path3, string path4, string path5, string path6, string path7) {
            string[] arrayPaths = new string[] { path1, path2, path3, path4, path5, path6, path7 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string Join (string path1, string path2, string path3, string path4, string path5, string path6, string path7, string path8) {
            string[] arrayPaths = new string[] { path1, path2, path3, path4, path5, path6, path7, path8 };
            return string.Join (_dirSeparator, arrayPaths);
        }

        public static string WebRequestPath (string path) {
            string prefix = "";

#if UNITY_STANDALONE_OSX
            prefix = "file:" + "//";
#endif

#if UNITY_IOS
            prefix = "file:" + "//";
#endif

            return prefix + path;
        }

        public static string DirSeparator { get => _dirSeparator; }
    }
}